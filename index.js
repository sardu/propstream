require('dotenv').config();
const APP_PATH = require('app-root-path');

const logger = require(APP_PATH + '/winston-config.js');
const puppeteer = require('puppeteer');
const fssync = require('fs');
const fs = require('fs').promises;
const AddressParser = require('parse-address');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const strftime = require('strftime');
const perf = require('execution-time')();
const argv = require('minimist')(process.argv.slice(2),
    {
        boolean: 'headless',
        default: {
            headless: null
        }
    });


const PropertyData = require('./src/PropertyData.js');
const RowData = require('./src/RowData.js');

async function login(page, username, password, cookiesFile) {
    await page.goto('https://login.propstream.com/', { waitUntil: "networkidle2" });

    await page.type('input[name=username]', username);
    await page.type('input[name=password]', password);
    await page.click('button[type=submit');
    await page.waitForNavigation({ timeout: 30000, waitUntil: 'networkidle2' });

    cookies = await page.cookies();
    logger.info(JSON.stringify(cookies, null, 2));
    await fs.writeFile(cookiesFile, JSON.stringify(cookies, null, 2));
}

async function navigateToSearch(page, cookiesFile) {
    let cookies = await fs.readFile(cookiesFile);
    try {
        cookies = JSON.parse(cookies);
    } catch (error) {
        cookies = [];
    }

    // logger.info(cookies);
    await page.setCookie(...cookies);
    await page.goto('https://app.propstream.com/search', { timeout: 30000, waitUntil: "networkidle2" });

    const url = page.url();
    if (url !== 'https://app.propstream.com/search') {
        return false;
    }

    // click Proceed on possible multiple login modal
    try {
        await page.waitForSelector('button._1MgdH__button.kQwFE__solid', { timeout: 1000 });
        await page.click('button._1MgdH__button.kQwFE__solid');
    } catch (error) {
        logger.info('Multiple Login modal not found, moving on');
    }

    // click Close on possible Software Update modal
    try {
        await page.waitForSelector('button._3FtuS__border-blue', { timeout: 1000 });
        await page.click('button._3FtuS__border-blue');
    } catch (error) {
        logger.info('Software Update modal not found, moving on');
    }

    return true;
}

async function touch(filename) {
    try {
        fssync.utimesSync(filename, time, time);
    } catch (err) {
        fssync.closeSync(fssync.openSync(filename, 'w'));
    }
};

function toRowData(propertyData) {
    logger.info('PropertyData', propertyData);
    let row = new RowData();

    // parse address
    if (propertyData.fullAddress) {
        const addy = AddressParser.parseLocation(propertyData.fullAddress);
        logger.info('Address', addy);
        row.address = formatAddress(addy);
        row.city = addy?.city;
        row.state = addy?.state;
        row.zipcode = addy?.zip;
    }

    // parse owner
    if (propertyData.owner) {
        let [lastName, ...firstName] = propertyData.owner.split(' ');
        firstName = firstName.join(' ');
        row.firstName = firstName;
        row.lastName = lastName;
    }

    // prase mail address
    if (propertyData.mailingAddress) {

        const mailAddy = AddressParser.parseLocation(propertyData.mailingAddress);
        logger.info('MailAddress', mailAddy);
        row.mailAddress = formatAddress(mailAddy);
        row.mailCity = mailAddy.city;
        row.mailState = mailAddy.state;
        row.mailZip = mailAddy.zip;
    }

    // parse property type
    row.propertyType = propertyData.propertyType;

    return row;
}

function formatAddress(address) {
    let formatAddress = `${address?.number || ''} ${address?.street || ''} ${address?.type || ''} ${address?.sec_unit_type || ''} ${address?.sec_unit_num || ''}`;
    formatAddress = formatAddress.trim();
    return formatAddress;
}

function initCsvWriter() {
    const dt = strftime('%Y%m%d-%H%M%S');
    const csvWriter = createCsvWriter({
        // path: `output-${dt}.csv`,
        path: `output.csv`,
        header: [
            // { id: 'input', title: 'Input' },
            { id: 'address', title: 'Address' },
            { id: 'city', title: 'City' },
            { id: 'state', title: 'State' },
            { id: 'zipcode', title: 'Zipcode' },
            { id: 'firstName', title: 'First Name' },
            { id: 'lastName', title: 'Last Name' },
            { id: 'mailAddress', title: 'Mail Address' },
            { id: 'mailCity', title: 'Mail City' },
            { id: 'mailState', title: 'Mail State' },
            { id: 'mailZip', title: 'Mail Zip' },
            { id: 'propertyType', title: 'Property Type' },
            { id: 'status', title: 'status' },
        ]
    });

    return csvWriter;
}

perf.start();
(async () => {
    const USERNAME = process.env.APP_USERNAME;
    const PASSWORD = process.env.APP_PASSWORD;

    // username and password check
    if (!USERNAME || !PASSWORD) {
        logger.error('Please provide an username and password in .env');
        process.exit(9);
    }

    // cookie file check
    const cookiesFile = './cookies.json';
    await touch(cookiesFile);

    const inputFile = await fs.readFile(APP_PATH + '/input.csv');
    const addresses = inputFile.toString().replace(/\r\n/g, '\n').split("\n");

    const browser = await puppeteer.launch({
        defaultViewport: null,
        devtools: true,
        headless: argv.headless ?? true,
        args: ['--start-maximized']
    });
    const page = await browser.newPage();
    page.setDefaultTimeout(10000);

    const csvWriter = initCsvWriter();

    // await login(page);
    logger.info('Starting Propstream search');
    const successful = await navigateToSearch(page, cookiesFile);
    if (!successful) {
        logger.info('Current login expired, attempting new login');
        await login(page, USERNAME, PASSWORD, cookiesFile);
        await navigateToSearch(page, cookiesFile);
    }

    for (let index = 0; index < addresses.length; index++) {
        let row = new RowData();

        const address = addresses[index];
        // const address = "1862 Court Ave, Memphis, TN ";
        logger.info('Input:', address);

        if (address.length === 0) {
            continue;
        }

        try {
            // type in search 
            const search = await page.waitForSelector('.react-autosuggest__container');
            await search.click({ clickCount: 3 });
            await page.type('.react-autosuggest__container > input[type=text]', address);
            await page.waitForTimeout(1000);

            await page.waitForFunction(() => {
                const element = document.querySelector('.react-autosuggest__suggestions-list > li');
                if (!element.textContent.includes('Loading')) {
                    console.log(element.textContent);
                    return true;
                }
            });

            const searchResultElement = await page.$('.react-autosuggest__suggestions-list > li');
            const searchResult = await page.evaluate(ele => ele.textContent, searchResultElement);
            logger.info('Found:', searchResult);


            if (searchResult.toLowerCase() === 'No data'.toLowerCase()) {
                logger.info('No search results');

                let propertyData = new PropertyData();
                propertyData.fullAddress = address;

                row = toRowData(propertyData);
                row.status = 'No Data';

            } else {
                logger.info('Looking up:', searchResult);

                let propertyData = new PropertyData();

                // click on search result
                await searchResultElement.click();

                // click for full property details
                const propertyDetailElement = await page.waitForSelector('a._2Zz6r__title');
                await propertyDetailElement.click();

                // wait for full detail modal
                await page.waitForFunction(() => {
                    const element = document.querySelector('._34QKo__headerTitle');
                    if (element.textContent !== '') {
                        console.log(element.textContent);
                        return true;
                    }
                });

                // get full address
                const fullAddress = await page.$eval('._34QKo__headerTitle', el => el.textContent);
                propertyData.fullAddress = fullAddress;

                // get owner and mail address
                const labels = await page.$$('._1TI2k__label');
                for (let i = 0; i < labels.length; i++) {
                    const label = labels[i];
                    const value = await page.evaluate(node => {
                        if (node.textContent == 'Owner 1 Name') {
                            return { owner: node.nextElementSibling.textContent };
                        } else if (node.textContent == 'Mailing Address') {
                            return { mailingAddress: node.nextElementSibling.textContent };
                        }
                    }, label);

                    propertyData = Object.assign({}, propertyData, value);
                }

                // get property type
                const anotherLabels = await page.$$('.R-NbA__label');
                for (let i = 0; i < anotherLabels.length; i++) {
                    const label = anotherLabels[i];
                    const value = await page.evaluate(node => {
                        if (node.textContent == 'Property Type:') {
                            return { propertyType: node.nextElementSibling.textContent };
                        }
                    }, label);
                    propertyData = Object.assign({}, propertyData, value);
                }

                row = toRowData(propertyData);
            }


        } catch (error) {
            logger.error('Error processing', error);

            let propertyData = new PropertyData();
            propertyData.fullAddress = address;

            row = toRowData(propertyData);
            row.status = error;
            // await page.waitForSelector('debug-zzzz');
        }

        await csvWriter.writeRecords([row]);

        // await page.waitForSelector('debug-zzzz');
        // await page.screenshot({ path: 'debug.png', fullPage: true });
        await page.keyboard.press('Escape');
        logger.info('done');
    }

    await browser.close();
    const timer = perf.stop();
    logger.info(timer.preciseWords);
})();