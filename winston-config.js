require('winston-daily-rotate-file');
const APP_PATH = require('app-root-path');
const winston = require('winston');
const util = require('util');
const strftime = require('strftime')

function utilFormatter() {
    return {
        transform: (info, opts) => {
            const args = info[Symbol.for('splat')];
            if (args) { info.message = util.format(info.message, ...args); }
            return info;
        }
    };
}

const logger = winston.createLogger({
    format: winston.format.combine(
        // winston.format.simple(),
        // winston.format.splat(),
        // winston.format.prettyPrint(),
        utilFormatter(),
        winston.format.timestamp({ format: () => { return strftime('%Y-%m-%d %H:%M:%S.%L%z') } }),
        // winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS z' }),
        winston.format.printf(({ level, message, label, timestamp }) => `${timestamp} ${label || '-'} ${level}: ${message}`),
    ),

    transports: [
        new winston.transports.Console({ handleExceptions: true }),
        new winston.transports.DailyRotateFile({
            filename: APP_PATH + '/logs/app-%DATE%.log',
            zippedArchive: true,
            maxFiles: '14d',
            createSymlink: true,
            symlinkName: 'app.log',
            handleExceptions: true,
        }),
    ],
    exitOnError: false
});

module.exports = logger;