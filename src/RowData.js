class RowData {
    address;
    city;
    state;
    zipcode;
    firstName;
    lastName;
    mailAddress;
    mailCity;
    mailState;
    mailZip;
    propertyType;
    status;
}

module.exports = RowData;